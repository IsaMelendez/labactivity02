package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter an integer");
        int newInt = scan.nextInt();

        System.out.println(Utilities.doubleMe(newInt));
    }
}
